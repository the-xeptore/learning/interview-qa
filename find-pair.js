const {isEqual} = require('lodash');

const array = [1, 5, 6, 5, 4, 1, 0, 1, 11, 52, 0, 3, 3, 3, 5, 4, 8, 2];

const target = 8;

function sortArray(array) {
  const clone = Array.of(...array);
  return clone.sort((a, b) => a - b);
}

function sortArray_test() {
  const array = [4, 3, 15, -1, 0, 48, -64];
  const sortedArray = sortArray(array);
  if (!isEqual(sortedArray, [-64, -1, 0, 3, 4, 15, 48])) {
    throw new Error('Wrong!');
  }
}

function findArrayBin(list, element) {
  let first = 0;
  let last = list.length - 1;
  let position = -1;
  let found = false;
  let middle;

  while (found === false && first <= last) {
    middle = Math.floor((first + last) / 2);
    if (list[middle] == element) {
      found = true;
      position = middle;
    } else if (list[middle] > element) {
      last = middle - 1;
    } else {
      first = middle + 1;
    }
  }
  return position;
}

function findArrayBin_test() {
  const list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  const position = findArrayBin(list, 5);
  if (position !== 5) {
    throw new Error('Wrong!');
  }
}

function matchFinderReducer(array, target) {
  return (acc, x) => {
    const searchingElement = target - x;
    const position = findArrayBin(array, searchingElement);
    const matchFound = position !== -1;
    if (matchFound) {
      return [...acc, [x, searchingElement]];
    }
    return [...acc];
  }
}

function matchFinderReducer_test() {
  const array = sortArray([1, 5, 4, 8, 6, 0, 7, 2, 9, 3]);
  const reducer = matchFinderReducer(array, 5);
  const result = array.reduce(reducer, []);
  if (!isEqual(result, [[1, 4]])) {
    console.error({result});
    throw new Error('Wrong!');
  }
}

function findPairUsingSearch(searchArray, sumTarget) {
  const sortedArray = sortArray(searchArray);
  const reducer = matchFinderReducer(sortedArray, sumTarget);
  const result = array.reduce(reducer, []);
  return result;
}

function findPairBruteForce(array, target) {
  const output = [];
  for (let i = 0; i < array.length; i++) {
    for (let j = i + 1; j < array.length; j++) {
      if (array[i] + array[j] === target) {
        output.push([array[i], array[j]]);
      }
    }
  }
  return output;
}

function findPair(array, target) {
  return findPairBruteForce(array, target);   // brute force approach
  return findPairUsingSearch(array, target);  // binary search approach
}

if (require.main === module) {
  const resultingPair = findPair(array, target);
  console.log(resultingPair)
}
